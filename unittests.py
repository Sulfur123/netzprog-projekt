#!/usr/bin/env python3
"""Module for unittest for quorum.py"""
import unittest
import logging as log
from quorum import get_quorum_info, form_master_opinion, form_quorum

class TestQuorum(unittest.TestCase):
    """Class with all relevant tests for quorum.py"""

    def setUp(self):
        pass

    def test_get_quorum_info_fallback(self):
        """Tests if the fallback is correctly used"""
        self.assertEqual(isinstance(get_quorum_info("foo"), float), True)

    def test_master_opionon_basic(self):
        """Tests the basic master opinion behaviour"""
        known_hosts_dummy = [
            {
                "quorum_info": 0.0,
                "status": 'online',
            },
            {
                "quorum_info": 1.0,
                "status": 'online',
            },
            {
                "quorum_info": 2.0,
                "status": 'online',
            }
        ]

        self.assertEqual(form_master_opinion(known_hosts_dummy), 0)

    def test_master_opionon_offline(self):
        """Tests the master opinion behaviour with some hosts offline"""
        known_hosts_dummy = [
            {
                "quorum_info": 0.0,
                "status": 'offline',
            },
            {
                "quorum_info": 1.0,
                "status": 'online',
            },
            {
                "quorum_info": 2.0,
                "status": 'online',
            }
        ]

        self.assertEqual(form_master_opinion(known_hosts_dummy), 1)

    def test_master_opionon_all_timestamp_vs_id(self):
        """Tests the master opinion behaviour with different quorum methods"""
        known_hosts_dummy = [
            {
                "quorum_info": 1562013740.3584735,
                "status": 'online',
            },
            {
                "quorum_info": 1562013742.3584735,
                "status": 'online',
            },
            {
                "quorum_info": 2.0,
                "status": 'online',
            }
        ]

        self.assertEqual(form_master_opinion(known_hosts_dummy), 2)

    def test_form_quorum_basic_master(self):
        """Tests the basic form quorum behaviour"""
        known_hosts_dummy = [
            {
                "quorum_info": 0.0,
                "status": 'online',
                "role": 'negotiating',
                "master_opinion": -1,
            },
            {
                "quorum_info": 1.0,
                "status": 'online',
                "role": 'negotiating',
                "master_opinion": 0,
            },
            {
                "quorum_info": 2.0,
                "status": 'online',
                "role": 'negotiating',
                "master_opinion": 0,
            }
        ]
        own_id = 0

        result_known_hosts = form_quorum(own_id, known_hosts_dummy)
        self.assertEqual(result_known_hosts[own_id]["role"], "master")

    def test_form_quorum_basic_worker(self):
        """Tests the basic form quorum behaviour with self as worker"""
        known_hosts_dummy = [
            {
                "quorum_info": 0.0,
                "status": 'online',
                "role": 'negotiating',
                "master_opinion": 0,
            },
            {
                "quorum_info": 1.0,
                "status": 'online',
                "role": 'negotiating',
                "master_opinion": -1,
            },
            {
                "quorum_info": 2.0,
                "status": 'online',
                "role": 'negotiating',
                "master_opinion": 0,
            }
        ]
        own_id = 1

        result_known_hosts = form_quorum(own_id, known_hosts_dummy)
        self.assertEqual(result_known_hosts[own_id]["role"], "worker")

    def test_form_quorum_basic_split_brain(self):
        """Tests the form quorum behaviour for the split brain case"""
        known_hosts_dummy = [
            {
                "quorum_info": 0.0,
                "status": 'online',
                "role": 'negotiating',
                "master_opinion": -1,
            },
            {
                "quorum_info": 1.0,
                "status": 'online',
                "role": 'negotiating',
                "master_opinion": 1,
            },
            {
                "quorum_info": 2.0,
                "status": 'online',
                "role": 'negotiating',
                "master_opinion": 2,
            }
        ]
        own_id = 0

        result_known_hosts = form_quorum(own_id, known_hosts_dummy)
        self.assertEqual(result_known_hosts[own_id]["role"], "split-brain")

    def test_form_quorum_outvoted(self):
        """Tests the form quorum behaviour if self is outvoted"""
        known_hosts_dummy = [
            {
                "quorum_info": 0.0,
                "status": 'online',
                "role": 'negotiating',
                "master_opinion": -1,
            },
            {
                "quorum_info": 1.0,
                "status": 'online',
                "role": 'negotiating',
                "master_opinion": 1,
            },
            {
                "quorum_info": 2.0,
                "status": 'online',
                "role": 'negotiating',
                "master_opinion": 1,
            }
        ]
        own_id = 0

        result_known_hosts = form_quorum(own_id, known_hosts_dummy)
        self.assertEqual(result_known_hosts[own_id]["role"], "worker")

    def test_form_quorum_not_enough_votes(self):
        """Tests the form quorum behaviour if not enough votes send"""
        known_hosts_dummy = [
            {
                "quorum_info": 0.0,
                "status": 'online',
                "role": 'negotiating',
                "master_opinion": -1,
            },
            {
                "quorum_info": 1.0,
                "status": 'offline',
                "role": 'negotiating',
                "master_opinion": 0,
            },
            {
                "quorum_info": 2.0,
                "status": 'offline',
                "role": 'negotiating',
                "master_opinion": 0,
            }
        ]
        own_id = 0

        result_known_hosts = form_quorum(own_id, known_hosts_dummy)
        self.assertEqual(result_known_hosts[own_id]["role"], "split-brain")


if __name__ == '__main__':
    print("Running Unit-Tests for quorum.py")
    # Log-Meldungen ausschalten
    log.basicConfig(format="%(levelname)s: %(message)s", level=log.CRITICAL)

    unittest.main()
