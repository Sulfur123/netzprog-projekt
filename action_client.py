#!/usr/bin/env python3
"""client-script to send action requests

Needs a running instance of quorum.py and a config file named config.ini
"""

import socket
import os
import sys
import logging as log
import argparse
from quorum import parse_config


if __name__ == "__main__":

    # logging and parsing
    log.basicConfig(format="%(levelname)s: %(message)s", level=log.DEBUG)

    PARSER = argparse.ArgumentParser(
        description=""" Client-script to send action requests.
                        \n Example: action_client.py 0"""
    )
    PARSER.add_argument(
        'ID_arg',
        metavar='ID',
        type=int,
        nargs=1,
        help='interger ID to indentify target server'
    )
    ARGS = PARSER.parse_args()
    ID = ARGS.ID_arg[0]

    if ID < 0:
        log.error("Given ID: %s is not greater or equal to 0", ID)
        sys.exit(1)

    KNOWN_HOSTS, ACTION, TICKRATE = parse_config(ID, 'config.ini')

    # send action command to target host
    HOST = KNOWN_HOSTS[ID]

    try:
        REMOTE_SERVER_SOCKET = socket.socket(
            socket.AF_INET,
            socket.SOCK_STREAM,
            socket.IPPROTO_TCP
        )
        REMOTE_SERVER_SOCKET.connect((HOST['host'], HOST['port']))

        log.info(
            "Sending action command to %s:%s",
            HOST['host'],
            HOST['port']
        )

        REMOTE_SERVER_SOCKET.send(bytes("ACTION".encode('utf-8')))
        RECIEVEDBYTES = REMOTE_SERVER_SOCKET.recv(100)
        ANSWER = RECIEVEDBYTES.decode('utf-8').split(":")

        if ANSWER[1] == "OK":
            log.info(
                "Server at %s:%s ran action successfully",
                HOST['host'],
                HOST['port']
            )
        else:
            log.warning(
                "Server at %s:%s failed to run action",
                HOST['host'],
                HOST['port']
            )

    except socket.error as error:
        log.warning(
            "Could not reach Host at %s:%s: %s",
            HOST['host'],
            HOST['port'],
            os.strerror(error.errno)
        )
        HOST['status'] = "offline"
    finally:
        REMOTE_SERVER_SOCKET.close()
