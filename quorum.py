#!/usr/bin/env python3
"""simple server client application for forming a quorum

quorum.py is a simple server/client application written as a programming
exercise with focus on network communicaiton. The application will try
to form a quorum(consenus) on the question of which instance should take
which role(worker/master). Requires a config-file named config.ini
for adress configuration.
"""

import socket
import sys
import configparser
import argparse
import time
import os
import copy
import subprocess
import logging as log
from threading import Thread


def get_quorum_info(quorum_method):
    """return the value needed for determining a quorum"""
    if quorum_method == 'start_time':
        return time.time()
    if quorum_method == 'ID':
        return float(ID)

    log.warning(
        "Config Error: no valid quorum_method, defaulting to start_time"
    )
    return time.time()


def form_master_opinion(hosts):
    """determine who should take on the master role"""
    current_master_opinion = -1

    # always take the lowest value, wether its ID or start_time
    lowest = float('inf')

    for i, host in enumerate(hosts):
        if (
                (
                    host['status'] == 'online' or
                    host['status'] == 'local'
                ) and
                host['quorum_info'] < lowest
        ):
            lowest = host['quorum_info']
            current_master_opinion = i

    return current_master_opinion


def form_quorum(own_id, hosts):
    """try to form a quorum based on the currently known hosts"""
    log.info("Forming new quorum")

    hosts[own_id]['master_opinion'] = form_master_opinion(hosts)

    # counting the votes of the hosts
    votes = [0] * len(hosts)
    total_votes = 0

    for host in hosts:
        if (
                (
                    host['status'] == 'online' or
                    host['status'] == 'local'
                ) and
                host['master_opinion'] >= 0
        ):
            total_votes += 1
            votes[host['master_opinion']] += 1

    # check if a quorum has been reached
    quorum_master = -1
    if total_votes > 1:
        for i, vote_count in enumerate(votes):
            if vote_count > total_votes / 2:
                print(
                    ("SUCCESS: quorum reached: new master is: {}"
                     " with votes: {} ({} needed)")
                    .format(i, votes, total_votes))
                quorum_master = i
    else:
        log.warning("not enough votes to reach quorum")

    if quorum_master >= 0:
        for i, host in enumerate(hosts):
            # only assign new roles to other hosts if they never had a value
            if (i == own_id or host['role'] == "negotiating"):
                if quorum_master == i:
                    host['role'] = 'master'
                else:
                    host['role'] = 'worker'
        log.info("new role: %s ", hosts[own_id]['role'])
    else:
        log.warning(
            "failed to reach quorum with votes: %s (%s needed)",
            votes, max(total_votes, 2)
        )
        hosts[own_id]['role'] = 'split-brain'

    return hosts


def server():
    """server-thread, responds to ping and action requests"""
    global KNOWN_HOSTS, STOPPED

    # make sure STOPPED is assigned a value
    if STOPPED:
        STOPPED = False

    try:
        server_socket = socket.socket(
            socket.AF_INET,
            socket.SOCK_STREAM,
            socket.IPPROTO_TCP
        )
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server_socket.settimeout(1)
        server_socket.bind((KNOWN_HOSTS[ID]['host'], KNOWN_HOSTS[ID]['port']))
        server_socket.listen(socket.SOMAXCONN)

        log.info(
            "Server%s started at %s:%s",
            ID, KNOWN_HOSTS[ID]['host'], KNOWN_HOSTS[ID]['port']
        )

        while not STOPPED:
            try:
                client_socket, addr = server_socket.accept()

                log.info("Incoming connection from %s", addr)

                recievedbytes = client_socket.recv(10)
                if not recievedbytes:
                    log.warning("Server received: nothing")

                cmd = recievedbytes.decode('utf-8')
                log.info("Server received: %s", cmd)

                if cmd == 'PING':
                    msg = "PING:{};{};{}".format(
                        KNOWN_HOSTS[ID]['quorum_info'],
                        KNOWN_HOSTS[ID]['role'],
                        KNOWN_HOSTS[ID]['master_opinion'])
                elif cmd == 'ACTION':
                    return_code = subprocess.call(ACTION, shell=True)
                    msg = 'ACTION:OK' if return_code == 0 else 'ACTION:NOTOK'
                else:
                    msg = "ERROR: Invalid Command"

                log.info("Server sending: %s", msg)
                client_socket.send(bytes(msg.encode('utf-8')))
                client_socket.close()
            except socket.timeout:
                pass    # to ensure a reaction to the STOPPED flag

    except socket.error as exception:
        log.error("Server Error: %s, exiting", str(exception))
    finally:
        STOPPED = True  # shut down the client gracefully
        server_socket.close()

    log.info(
        "Server%s shutting down at %s:%s",
        ID, KNOWN_HOSTS[ID]['host'], KNOWN_HOSTS[ID]['port']
    )


def client():
    """client-thread, checks known hosts for changes(ping requests)"""
    global KNOWN_HOSTS, STOPPED

    while not STOPPED:
        old_known_hosts = copy.deepcopy(KNOWN_HOSTS)

        # ping all known hosts
        for host in KNOWN_HOSTS:
            # dont ping self
            if host['status'] == 'local':
                continue
            try:
                remote_server_socket = socket.socket(
                    socket.AF_INET,
                    socket.SOCK_STREAM,
                    socket.IPPROTO_TCP
                )
                remote_server_socket.connect((host['host'], host['port']))

                log.info(
                    "Pinging Server at %s:%s",
                    host['host'], host['port']
                )

                remote_server_socket.send(bytes("PING".encode('utf-8')))

                recievedbytes = remote_server_socket.recv(100)
                if not recievedbytes:
                    log.warning("Empty Answer to PING command")
                    host['status'] = "offline"
                else:
                    answer = recievedbytes.decode('utf-8').split(":")
                    answer_data = answer[1].split(";")

                    host['status'] = "online"
                    host['quorum_info'] = float(answer_data[0])
                    host['role'] = answer_data[1]
                    host['master_opinion'] = int(answer_data[2])

                    log.info("Client Recvieved: %s", answer[1])

            except socket.error as error:
                log.warning(
                    "host at %s:%s: %s",
                    host['host'],
                    host['port'],
                    os.strerror(error.errno)
                )
                host['status'] = "offline"
            except Exception as exception:
                log.error("Client Error: %s, exiting", str(exception))
                STOPPED = True   # shut down the server gracefully
            finally:
                remote_server_socket.close()

        # try to form a new quorum if changes occured
        if check_for_changes(KNOWN_HOSTS, old_known_hosts):
            KNOWN_HOSTS = form_quorum(ID, KNOWN_HOSTS)

        time.sleep(TICKRATE)

    log.info(
        "Client%s shutting down at %s:%s",
        ID,
        KNOWN_HOSTS[ID]['host'],
        KNOWN_HOSTS[ID]['port']
    )


def check_for_changes(known_hosts, old_known_hosts):
    """determine if any changes occured in the observed hosts"""
    for i, host in enumerate(known_hosts):
        for key in host:
            if host[key] != old_known_hosts[i][key]:
                # try to form a new quorum
                log.info(
                    "Found new status at %s:%s: key %s new: %s old: %s",
                    host['host'],
                    host['port'],
                    key,
                    host[key],
                    old_known_hosts[i][key]
                )
                return True
    return False


def parse_arguments():
    """helper function to parse arguments passed to the application"""
    parser = argparse.ArgumentParser(
        description="""Simple server client application for forming a
                        quorum with seperate instances of itself over
                        a network. Requires a config-file named config.ini
                        for adress configuration.
                        \n Example: python3 quorum.py -v 0"""
    )

    parser.add_argument(
        'ID_arg',
        metavar='ID',
        type=int,
        nargs=1,
        help='interger ID to indentify this instance'
    )

    parser.add_argument(
        '-v',
        '--verbose',
        action='count',
        help='activates verbose output'
    )

    args = parser.parse_args()

    if args.verbose:
        log.basicConfig(format="%(levelname)s: %(message)s", level=log.DEBUG)
        log.info("Verbose output.")
    else:
        log.basicConfig(format="%(levelname)s: %(message)s")

    own_id = args.ID_arg[0]
    if own_id < 0:
        log.error("Given ID: %s is not greater or equal to 0", ID)
        sys.exit(1)

    return own_id


def parse_config(own_id, config_file_path):
    """helper function, to parse the config file"""
    hosts = []
    config = configparser.ConfigParser()
    try:
        with open(config_file_path) as config_file:
            config.read_file(config_file)

            action = config['DEFAULT']['action']
            tickrate = int(config['DEFAULT']['tickrate'])

            # read/init values
            for entry in config.sections():
                host = {
                    "host": config[entry]['host'],
                    "port": int(config[entry]['port']),
                    "quorum_info": float('inf'),
                    "status": 'offline',
                    "role": 'negotiating',
                    "master_opinion": -1,
                    }
                hosts.append(host)

            hosts[own_id]['quorum_info'] = get_quorum_info(
                config['DEFAULT']['quorum_method']
            )
            hosts[own_id]['status'] = 'local'

    except IndexError as exception:
        log.error(
            ("Config Error: Config file allows ID"
             " parameter to be between 0 and %s, got %s: %s"),
            len(hosts)-1,
            own_id,
            exception
        )
        sys.exit(1)

    except KeyError as exception:
        log.error("Config Error: Missing Config Entry %s", exception)
        sys.exit(1)

    except (FileNotFoundError, IOError) as exception:
        log.error("Config Error: Couldnt open config: %s", exception)
        sys.exit(1)

    return hosts, action, tickrate


if __name__ == "__main__":
    ID = parse_arguments()
    KNOWN_HOSTS, ACTION, TICKRATE = parse_config(ID, 'config.ini')

    STOPPED = False     # flag for stopping server and client together

    # starting server/client
    SERVER_THREAD = Thread(target=server)
    CLIENT_THREAD = Thread(target=client)

    try:
        SERVER_THREAD.start()
        time.sleep(0.1)     # let server bind to port first
        CLIENT_THREAD.start()

        while SERVER_THREAD.isAlive and not STOPPED:
            SERVER_THREAD.join()    # to be able to catch keyboard interrupts
    except KeyboardInterrupt:
        print("Caught keyboard interrupt, exiting now")
        STOPPED = True
