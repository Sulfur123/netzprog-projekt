This is a project written for the course Netzwerkprogrammierung SS 2019 at Bielefeld University.

The main part is the script quorum.py, a threaded server/client application that is able to form a quorum on the distribution of roles with seperatly running instances of itself over a network.
With the config file config.ini its possible to configure the network adresses, tick rates, action to be performed by the quorum.py server and the method to be used for determining a quorum.
Possible methods are ID or start_time, in each case the lowest value is assigned the master role.
Also included are unittests.py (for unit tests) and action_client.py (simple client to query configured action commands on a running quorum.py server). 

With the defaul configuration file an example usage would be:
(each in a seperate shell)
quorum.py -v 0
quorum.py -v 1
quorum.py -v 2

action_client.py 1 (to query the configured action on server 1)
